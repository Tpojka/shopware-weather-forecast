<?php
/**
 * Project shopware6.test
 * File: FrontendSubscriber.php
 * Created by: tpojka
 * On: 20. 12. 2021.
 */

namespace Tpojka\WeatherForecast\Subscriber;

use Shopware\Storefront\Page\Product\ProductPageLoadedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Tpojka\WeatherForecast\Service\Metno\MetnoFactory;

class FrontendSubscriber implements EventSubscriberInterface
{
    /**
     * @var MetnoFactory
     */
    private MetnoFactory $metnoFactory;
    
    public function __construct(MetnoFactory $metnoFactory)
    {
        $this->metnoFactory = $metnoFactory;
    }
    
    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            ProductPageLoadedEvent::class => 'onRequest', 
        ];
    }

    /**
     * @param ProductPageLoadedEvent $requestEvent
     * @return void
     */
    public function onRequest(ProductPageLoadedEvent $requestEvent)
    {
        dd($requestEvent->getRequest()->getClientIp());
    }
}
