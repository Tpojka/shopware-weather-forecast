<?php
/**
 * Project METno
 * File: MetnoInterface.php
 * Created by: tpojka
 * On: 18. 12. 2021.
 */

namespace Tpojka\WeatherForecast\Service\Metno\Contract;

interface MetnoInterface
{
    const DOWNLOAD_FAILED   = 100;
    const XML_INVALID       = 101;
    const DATA_EMPTY        = 102;
    const FORECAST_INVALID  = 103;
}
